// const { useState } = require("react")
// const {useState } = React

const LeftColumn = (props) => {

    let topLikedComments = [];
    let topDislikedComments = [];
    let comments = [];
    props.listComments.map((comment) => (
        comments.push(JSON.parse(JSON.stringify(comment)))
    ))

    let getNumComments = (user) => {
        let count = 0;
        for (let i = 0; i < comments.length; i++) {
            if(comments[i].user === user){
                count += 1;
            }
        }
        return count;
    }

    let getTopComments = () => {
        comments.sort((a, b) => (a.likes < b.likes ? 1 : -1));
        comments.map((comment) =>(
            comment.count = getNumComments(comment.user),
            topLikedComments.push(JSON.parse(JSON.stringify(comment)))
        ))
    }

    let getBottomComments = () => {
        comments.sort((a, b) => (a.unlikes < b.unlikes ? 1 : -1));
        comments.map((comment) => (
            comment.count = getNumComments(comment.user),
            topDislikedComments.push(JSON.parse(JSON.stringify(comment)))
        ))
    }

    getTopComments();
    getBottomComments();
    topLikedComments.splice(5, topLikedComments.length);
    topDislikedComments.splice(5, topDislikedComments.length);
    return ( <section id="left-column"> 
                <h3>Some Stats</h3>
                <h3>top 5 liked comments</h3>
                <table className="topLikedTable">
                    <tbody>
                        <tr>
                            <th>name</th>
                            <th>nbComments</th>
                        </tr>
                        {
                            topLikedComments.map((comment) =>(
                                <tr>
                                    <td>{comment.user}</td>
                                    <td>{comment.count}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                <h3>top 5 unliked comments</h3>
                <table>
                    <tbody>
                        
                        <tr>
                            <th>name</th>
                            <th>nbcomments</th>
                        </tr>
                        {
                            topDislikedComments.map((comment) =>(
                                <tr>
                                    <td>{comment.user}</td>
                                    <td>{comment.count}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
        </section>  );
}
 
// export default LeftColumn;