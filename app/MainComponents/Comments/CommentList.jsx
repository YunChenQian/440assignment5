const CommentList = (props) => {
    return ( 
        <section id="list-tasks">

        {
        props.listComments.map ((task)=>(
            <section className="task-wrapper"  key = {task.id}>
            <section className="task-row">
                <section className="task-name">{task.user}</section>
                <section className="task-date">{task.date}</section>
            </section>

            <section className="task-row">
                <section className="task-text">{task.text}</section>
                <section className="task-likes">< div className="imgContainer"><img src="icons\like.PNG"/></div>{task.likes}</section>
                <section className="task-dislikes"><div className="imgContainer"><img src="icons\unlike.PNG"/></div>{task.unlikes}</section>
                <section className="task-flags"><div className="imgContainer"><img src="icons\flag.png"/></div>{task.flag}</section>
                <section className="task-del"><div className="imgContainer"><img src="icons\minus.png"/></div><button onClick={()=>props.handledeleteTask(task.id)}>del</button></section>
                
            </section>
        </section>

        )) 
        }
    </section>
    )}
 
//export default CommentList;