// const { useState } = require("react")
// const {useState } = React

const RightColumn = (props) => {

    let filteredComments = [];
    let comments = [];
    props.listComments.map((comment) => (
        comments.push(JSON.parse(JSON.stringify(comment)))
    ))

    let getFlaggedComments = () => {
        comments.sort((a, b) => (a.likes < b.likes ? 1 : -1));
        comments.map((comment) =>
            {
                if (comment.flag > 0){
                    filteredComments.push(JSON.parse(JSON.stringify(comment)));
                }
            }
        )
    }


    getFlaggedComments();

    return ( <section id="left-column"> 
                <h3>Some Stats</h3>
                <h3>top 5 liked comments</h3>
                <table className="topLikedTable">
                    <tbody>
                        <tr>
                            <th>name</th>
                            <th>liked</th>
                            <th>unliked</th>
                        </tr>
                        {
                            filteredComments.map((comment) =>(
                                <tr>
                                    <td>{comment.user}</td>
                                    <td>{comment.likes}</td>
                                    <td>{comment.unlikes}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>    
        </section>  );
}
 
// export default LeftColumn;