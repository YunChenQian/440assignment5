const Main = () => {
    let commentsData = [
        {id:1, user:'twain', text:'God created war so that Americans would learn geography.', date:'2/23/2011', 
       likes:5, unlikes:25, flag:0},
        {id:2, user:'Andy', text:'On the backend, Facebook builds their application in Ruby on Rails which allows them to have automatic type handling for their templates which is made possible by ActiveModel. For all the great!', 
       date:'3/29/2015', likes:16, unlikes:4, flag:0},
        {id:3, user:'wisdom', text:'But if they desist, then know that hostility is only against the wrong-doers.', 
       date:'8/14/2015', likes:19, unlikes:5, flag:0},
        {id:4, user:'Maral', text:'Database - In fact, Facebook has made this a pillar of their architecture. They built it in order to have correct data management from the server side to the client.', date:'2/26/2020', likes:20, unlikes:2, 
       flag:1},
        {id:5, user:'hubert', text:'Older men declare war. But it is youth that must fight and die.', date:'4/27/2023', 
       likes:19, unlikes:4, flag:0},
        {id:6, user:'Spencer', text:'So, we have defined a Todo Model. It has records for Todo and Value. The data is guaranteed to be consistent as it changes from the server to the client.', date:'2/9/2020', likes:22, unlikes:25, 
       flag:0},
        {id:7, user:'Wenpei', text:'Java is a programming language used to develop software.', date:'9/2/2018', 
       likes:13, unlikes:1, flag:1},
        {id:8, user:'ahmed', text:'Apache was originally developed in 1993 for the Perl programming language to provide an HTML parser.', date:'2/9/2018', likes:11, unlikes:2, flag:1},
        {id:9, user:'chun', text:'Java was born from the fact that Perl syntax was proving too complex to implement in Linux.', date:'4/28/2014', likes:1, unlikes:20, flag:1},
        {id:10, user:'Jane', text:'React is a JS library that is supposed to make web applications more efficient since rerendering is done at client side rather than fetching from server side.', date:'10/15/2020', likes:18, unlikes:18, 
       flag:1},
        {id:11, user:'two-towers', text:'War must be, while we defend our lives against a destroyer who would devour all; but I do not love the bright sword for its sharpness, nor the arrow for its swiftness, nor the warrior for his glory. I love only that which they defend.', date:'4/20/2011', likes:1, unlikes:24, flag:0},
        {id:12, user:'voltaire', text:'It is forbidden to kill; therefore all murderers are punished unless they kill in large numbers and to the sound of trumpets.', date:'1/9/2022', likes:4, unlikes:21, flag:1},
        {id:13, user:'Su Bin', text:'Model, Collection, Hash and Array all provide data integrity and guarantee that all the data changes are propagated across the application.', date:'7/1/2011', likes:3, unlikes:17, flag:1},
        {id:14, user:'Ilaria', text:'Response - In this case, it is a function that is used to tell a client what it got from the server. This is made possible by ActiveRecord.', date:'6/15/2018', likes:9, unlikes:22, flag:1},
        {id:15, user:'tzu', text:'The supreme art of war is to subdue the enemy without fighting.', date:'9/7/2011', 
       likes:17, unlikes:23, flag:0}]

    let [listComments, setListTasks] = React.useState(commentsData)

    const handledeleteTask = (Id)=>{
        let newList = listComments.filter( (tsk)=>tsk.id!==Id )
        console.log("newList=", newList)
       setListTasks(listComments.filter( (tsk)=>tsk.id!==Id ))
    }

    // const handleLikedTask = (Id)=>{
    //     let newList = [];
    //     listComments.map( (tsk) =>  {
    //         if(tsk.id == Id){
    //             console.log("add");
    //             tsk.likes += 1;
    //         }
    //         newList.push(JSON.parse(JSON.stringify(tsk)));
    //     })
    //     setListTasks({newList});
    // }

    return ( <section id="main">
         {/* main here (external - export) */}
    <LeftColumn listComments = {listComments}/>
    <MiddleColumn listComments = {listComments} handledeleteTask = {handledeleteTask}/> 
    <RightColumn listComments = {listComments}/>
    </section> );
}
 
// export default Main;