FROM httpd
LABEL mainttainer="YunChenQian" email="yun.qian@dawsoncollege.qc.ca"
# DocumentRoot
WORKDIR /usr/local/apache2/htdocs/

# copy app into image
COPY app/ ./

# expose port 80
EXPOSE 80
